create TABLE users (
                       id bigint not null unique primary key,
                       created_by varchar,
                       created_when timestamp,
                       first_name varchar not null ,
                       last_name varchar not null ,
                       middle_name varchar,
                       date_birth date not null ,
                       phone varchar not null ,
                       address varchar,
                       backup_email varchar not null ,
                       login varchar not null unique ,
                       password varchar not null,
                       role_id bigint REFERENCES roles(id)
);
create sequence users_id_seq increment 1 start 100 OWNED BY users.id;
alter table users
    alter COLUMN id SET DEFAULT nextval('users_id_seq');

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (1, 'Serge I', '2022-11-09 16:08:41.000000', 'Антон', 'Петров', null, '1999-01-13', '89993422123',
        'Петровка 34 кв 3', 'apetrov@mail.ru', 'apetrov', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 1);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (2, 'Serge I', '2022-11-09 16:08:41.000000', 'Иван', 'Гаврилов', null, '1988-01-23', '89675432154',
        'Коровина 67 кв 8', 'igavrilov@ya.ru', 'igavrilov', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 1);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (3, 'Serge I', '2022-11-09 16:08:41.000000', 'Гриша', 'Валов', null, '1989-01-19', '89344561232',
        'пр.Ленина 55 кв 233', 'gvalov@mail.ru', 'gvalov', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 2);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (4, 'Serge I', '2022-11-09 16:08:41.000000', 'Юлия', 'Сазонова', null, '1991-11-23', '89843422123',
        'Гоголя 587 кв 1', 'ysazonova@mail.ru', 'ysazonova', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 2);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (5, 'Serge I', '2022-11-09 16:08:41.000000', 'Ольга', 'Иванова', null, '1981-03-15', '89993562123',
        'Орбита 98 кв 343', 'oivanova@mail.ru', 'oivanova', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 1);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (6, 'Maksim C', '2022-11-09 16:08:41.000000', 'Сергей', 'Логинов', null, '2000-01-13', '89883492123',
        'Люлина 4 кв 3', 'sloginov@mail.ru', 'sloginov', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 2);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (7, 'Maksim C', '2022-11-09 16:08:41.000000', 'Олеся', 'Ванина', null, '1997-06-23', '89453422123',
        'Липовая 4 кв 323', 'ovanina@mail.ru', 'ovanina', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 1);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (8, 'Maksim C', '2022-11-09 16:08:41.000000', 'Гоги', 'Иванишвилли', null, '1994-08-05', '89233454123',
        'Вилюйская 76 кв 95', 'givanish@mail.ru', 'givanish', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 1);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (9, 'Maksim C', '2022-11-09 16:08:41.000000', 'Уль', 'Второй', null, '1993-02-09', '89753422198',
        'Дубовая 76 кв 94', 'uvtoroi@ya.ru', 'uvtoroi', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 1);

INSERT INTO users (id, created_by, created_when, first_name, last_name, middle_name, date_birth, phone, address,
                          backup_email, login, password, role_id )
VALUES (10, 'Maksim C', '2022-11-09 16:08:41.000000', 'Генадий', 'Красный', null, '2000-03-30', '89113422111',
        'Розовая 544 кв 8', 'gred@mail.ru', 'gred', '$2a$10$IRAa2L42Bz011.za0K6QrevQX2cGHIsbfouKkGdP9OU6S6klTsJn6', 1);