create table recording
(
    id           bigint not null
        constraint recording_pkey
            primary key,
    created_by   varchar(255),
    created_when timestamp,
    box_number   varchar(255),
    date_it      date,
    end_time     time,
    start_time   time,
    user_id      bigint
        constraint fklqcgvoyu2gbppwrqeon1we5s0
            references users
);

create sequence recording_id_seq increment 1 start 100 OWNED BY recording.id;
alter table recording
    alter COLUMN id SET DEFAULT nextval('recording_id_seq');

INSERT INTO recording (id, created_by, created_when, box_number, date_it, end_time, start_time, user_id) VALUES (12, null, '2023-01-19 10:52:28.552231', '1', date(now()), '12:15:00', '12:00:00', 1);
INSERT INTO recording (id, created_by, created_when, box_number, date_it, end_time, start_time, user_id) VALUES (13, null, '2023-01-19 11:40:56.098731', null, date(now()), '13:30:00', '13:00:00', 1);
INSERT INTO recording (id, created_by, created_when, box_number, date_it, end_time, start_time, user_id) VALUES (14, null, '2023-01-19 11:43:57.389231', null, date(now()), '13:00:00', '12:15:00', 1);
INSERT INTO recording (id, created_by, created_when, box_number, date_it, end_time, start_time, user_id) VALUES (15, null, '2023-01-19 14:14:52.328163', null, date(now()), '15:45:00', '15:00:00', 1);