create table service
(
    id           bigint not null
        constraint service_pkey
            primary key,
    created_by   varchar(255),
    created_when timestamp,
    description  varchar(255),
    price        double precision,
    take_time    integer,
    title        varchar(255)
);

create sequence service_id_seq increment 1 start 100 OWNED BY service.id;
alter table recording
    alter COLUMN id SET DEFAULT nextval('service_id_seq');

INSERT INTO service (id, created_by, created_when, description, price, take_time, title) VALUES (1, null, null, null, 30, 30, 'Помывка');
INSERT INTO service (id, created_by, created_when, description, price, take_time, title) VALUES (2, null, null, null, 15, 15, 'Протирка');
INSERT INTO service (id, created_by, created_when, description, price, take_time, title) VALUES (3, null, null, null, 25, 30, 'Пропылесосить');