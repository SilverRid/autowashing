create table service_recording
(
    service_id   bigint not null
        constraint service_recording_service_id_fkey
            references recording,
    recording_id bigint not null
        constraint service_recording_recording_id_fkey
            references service,
    constraint service_recording_pkey
        primary key (service_id, recording_id)
);

INSERT INTO service_recording (service_id, recording_id) VALUES (12, 2);
INSERT INTO service_recording (service_id, recording_id) VALUES (13, 1);
INSERT INTO service_recording (service_id, recording_id) VALUES (14, 2);
INSERT INTO service_recording (service_id, recording_id) VALUES (14, 1);
INSERT INTO service_recording (service_id, recording_id) VALUES (15, 1);
INSERT INTO service_recording (service_id, recording_id) VALUES (15, 2);