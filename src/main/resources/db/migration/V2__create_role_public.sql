create TABLE roles (id bigint not null unique primary key,
                       title varchar(20) unique not null,
                       description varchar
);
create sequence roles_id_seq increment 1 start 100 OWNED BY roles.id;
alter table roles
    alter COLUMN id SET DEFAULT nextval('roles_id_seq');

INSERT INTO roles (id, title, description)
VALUES (1, 'ROLE_USER', 'Клиент');
INSERT INTO roles (id, title, description)
VALUES (2, 'ROLE_ADMIN', 'Администратор');