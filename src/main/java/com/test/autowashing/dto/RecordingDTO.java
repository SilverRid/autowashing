package com.test.autowashing.dto;

import java.util.Set;
import com.test.autowashing.model.Recording;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RecordingDTO extends RecotdingBasicDTO{
    private Long user;
    private Set<Long> services;
}
