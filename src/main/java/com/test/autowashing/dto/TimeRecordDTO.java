package com.test.autowashing.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import com.test.autowashing.model.Recording;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TimeRecordDTO {
    private LocalDate dateIt;
    private LocalTime startTime;
    private LocalTime endTime;

    public TimeRecordDTO(final Recording record) {
        this.dateIt = record.getDateIt();
        this.endTime = record.getEndTime();
        this.startTime = record.getStartTime();
    }
}
