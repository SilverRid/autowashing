package com.test.autowashing.dto;

import java.util.Set;
import com.test.autowashing.model.Recording;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RecordingWithNamesAndRemainTimeDTO extends RecotdingBasicDTO {
    private String user;
    private Set<String> services;
    private Integer remainTime;

    public RecordingWithNamesAndRemainTimeDTO(final Recording recording) {
        super(recording);
    }
}
