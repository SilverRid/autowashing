package com.test.autowashing.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ServiceAutoDTO {
    private Long id;
    private String title;
    private String description;
    private Double price;
    private Integer takeTime;
}
