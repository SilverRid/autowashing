package com.test.autowashing.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import com.test.autowashing.model.Recording;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RecotdingBasicDTO {
    private Long id;
    @Schema(pattern = "HH:mm", example = "12:00")
    private LocalTime startTime;
    private LocalDate dateIt;
    private String boxNumber;

    public RecotdingBasicDTO(final Recording recording) {
        this.id = recording.getId();
        this.startTime = recording.getStartTime();
        this.dateIt = recording.getDateIt();
        this.boxNumber = recording.getBoxNumber();
    }
}
