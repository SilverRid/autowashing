package com.test.autowashing.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import com.test.autowashing.dto.TimeRecordDTO;
import com.test.autowashing.model.Recording;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Repository
public interface RecordingRepository extends JpaRepository<Recording, Long> {

    @Query(nativeQuery = true,value = """
        select max(end_time) from recording where date_it = date(now());
        """)
    Optional<LocalTime> getLastTime();

    @Query(nativeQuery = true,value = """
        select count(*) from recording where date_it = :dateIt and 
        (((start_time < :startTime and :startTime < end_time) OR (start_time < :endTime and :endTime < end_time) ) OR
        ((:startTime < start_time and start_time < :endTime) OR (:startTime < end_time and end_time < :endTime)))
        ;
        """)
    Integer findBetweenTime(@Param(value = "dateIt") LocalDate dateIt,
                            @Param(value = "startTime") LocalTime startTime,
                            @Param(value = "endTime") LocalTime endTime);

    @Query(nativeQuery = true,value = """
        select * from recording where  date_it = :date order by start_time;
        """)
    List<Recording> findAllbyDate(@Param(value = "date") LocalDate date);
}
