package com.test.autowashing.repository;

import com.test.autowashing.model.ServiceAuto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Repository
public interface ServiceRepository extends JpaRepository<ServiceAuto, Long> {
}
