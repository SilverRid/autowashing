package com.test.autowashing.repository;

import com.test.autowashing.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String username);
}
