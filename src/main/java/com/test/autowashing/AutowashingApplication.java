package com.test.autowashing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutowashingApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutowashingApplication.class, args);
    }

}
