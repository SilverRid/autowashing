package com.test.autowashing.controller;

import java.util.List;
import com.test.autowashing.dto.ServiceAutoDTO;
import com.test.autowashing.model.ServiceAuto;
import com.test.autowashing.service.ServiceAutoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@RestController
@RequestMapping(value = "/service")
@Tag(name = "Сервис", description = "Контроллер для работы с услугами помывки автомобиля")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ServiceController {

    private ServiceAutoService serviceAutoService;

    public ServiceController(final ServiceAutoService serviceAutoService) {
        this.serviceAutoService = serviceAutoService;
    }

    @Operation(description = "Получаем сервис по id")
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceAuto> getOne(@RequestParam(name = "id") Long id) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(serviceAutoService.getOne(id));
    }

    @Operation(description = "Получаем список услуг")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ServiceAuto>> list() {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(serviceAutoService.listAll());
    }

    @Operation(description = "Добавляем сервис")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceAuto> create(@RequestBody ServiceAutoDTO serviceAutoDTO) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(serviceAutoService.createFromDTO(serviceAutoDTO));
    }

    @Operation(description = "Обновляем сервис")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServiceAuto> update(@RequestBody ServiceAutoDTO serviceAutoDTO, @RequestParam(name = "id") Long id) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(serviceAutoService.updateFromDTO(serviceAutoDTO, id));
    }

    @Operation(description = "Удаляем сервис по id")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(name = "id") Long id) {
        serviceAutoService.delete(id);
        return ResponseEntity
            .status(HttpStatus.OK)
            .body("Сервис с id = " + id + " был удален!");
    }
}
