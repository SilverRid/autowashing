package com.test.autowashing.controller;

import java.time.LocalDate;
import java.util.List;
import com.test.autowashing.dto.RecordingDTO;
import com.test.autowashing.dto.RecordingWithNamesAndRemainTimeDTO;
import com.test.autowashing.dto.TimeRecordDTO;
import com.test.autowashing.model.Recording;
import com.test.autowashing.service.RecordingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@RestController
@RequestMapping(value = "/recording")
@Tag(name = "Запись", description = "Контроллер для записи на мойку автомобиля")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RecordingController {

    private RecordingService recordingService;

    public RecordingController(final RecordingService recordingService) {
        this.recordingService = recordingService;
    }

    @Operation(description = "Добавляем запись на определенное время")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Recording> create(@RequestBody RecordingDTO recordingDTO) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(recordingService.createFromDTO(recordingDTO));
    }

    @Operation(description = "Удаляем запись по id")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(name = "id") Long id) {
        recordingService.delete(id);
        return ResponseEntity
            .status(HttpStatus.OK)
            .body("\"Запись с id = " + id + " удалена!\"");
    }

    @Operation(description = "Получаем список записей")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Recording>> list() {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(recordingService.listAll());
    }

    @Operation(description = "Получаем список свободного времени")
    @RequestMapping(value = "/listFreeTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TimeRecordDTO>> listFreeTime(@RequestParam(name = "date") String dateString) {
        LocalDate date = LocalDate.parse(dateString);
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(recordingService.listFreeTime(date));
    }

    @Operation(description = "Получаем список занятого времени")
    @RequestMapping(value = "/listBookTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TimeRecordDTO>> listBookTime(@RequestParam(name = "date") String dateString) {
        LocalDate date = LocalDate.parse(dateString);
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(recordingService.listBookTime(date));
    }

    @Operation(description = "Получаем запись c временем ожидания")
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RecordingWithNamesAndRemainTimeDTO> getOne(@RequestParam(name = "id") Long id) {
        return ResponseEntity
            .status(HttpStatus.OK)
            .body(recordingService.getOneRecordingWNART(id));
    }
}
