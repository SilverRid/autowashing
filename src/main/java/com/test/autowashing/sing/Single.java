package com.test.autowashing.sing;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
public class Single {

    private static volatile Single single = null;

    private Single() {
    }

    public static Single getSingle() {
        if (single == null) {
            synchronized (Single.class) {
                if (single == null) {
                    single = new Single();
                    return single;
                }
            }
        }
        return single;
    }
}
