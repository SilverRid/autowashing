package com.test.autowashing.exeption;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Slf4j
@ControllerAdvice
public class ExceptionControllerREST {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleError(HttpServletRequest request, Exception e) {
        log.error("Something wrong!!!  " +
            request.getAttribute(RequestDispatcher.ERROR_EXCEPTION) + " " +
            request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
        log.error(e.getClass() + " " + e.getMessage());
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("{ Error : " + e.getMessage() + " } ");
    }
}
