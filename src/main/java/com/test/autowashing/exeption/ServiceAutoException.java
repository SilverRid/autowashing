package com.test.autowashing.exeption;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
public class ServiceAutoException extends RuntimeException{
    public ServiceAutoException(final String message) {
        super(message);
    }
}
