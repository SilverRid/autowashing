package com.test.autowashing.exeption;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
public class RecordingException extends RuntimeException{
    public RecordingException(final String message) {
        super(message);
    }
}
