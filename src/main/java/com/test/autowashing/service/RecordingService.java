package com.test.autowashing.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import com.test.autowashing.dto.RecordingDTO;
import com.test.autowashing.dto.RecordingWithNamesAndRemainTimeDTO;
import com.test.autowashing.dto.TimeRecordDTO;
import com.test.autowashing.exeption.RecordingException;
import com.test.autowashing.model.Recording;
import com.test.autowashing.model.ServiceAuto;
import com.test.autowashing.repository.RecordingRepository;
import com.test.autowashing.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.webjars.NotFoundException;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Log4j2
@Service
public class RecordingService extends GenericService<Recording, RecordingDTO> {

    private RecordingRepository recordingRepository;

    private ServiceAutoService serviceAutoService;

    private UserRepository userRepository;

    public RecordingService(final RecordingRepository recordingRepository,
                            final ServiceAutoService serviceAutoService,
                            final UserRepository userRepository) {
        this.recordingRepository = recordingRepository;
        this.serviceAutoService = serviceAutoService;
        this.userRepository = userRepository;
    }

    @Override
    public Recording updateFromEntity(final Recording object) {
        return null;
    }

    @Override
    public Recording updateFromDTO(final RecordingDTO object, final Long objectId) {
        return null;
    }

    @Override
    public Recording createFromEntity(final Recording newObject) {
        return null;
    }

    @Override
    @Transactional
    public Recording createFromDTO(final RecordingDTO newObject) {

        Recording recording = new Recording();

        if (newObject.getStartTime().isAfter(LocalTime.of(22, 0)) &&
            newObject.getStartTime().isBefore(LocalTime.of(6, 0))) {
            log.error("RecordingService.createFromDTO: На данное время записи нет (рабочий день с 6:00 до 22:00)");
            throw new RecordingException("На данное время записи нет (рабочий день с 6:00 до 22:00)");
        }
        recording.setStartTime(newObject.getStartTime());


        Set<ServiceAuto> serviceAutos = serviceAutoService.setFromListId(newObject.getServices());
        Integer takeTime = serviceAutos.stream().map(ServiceAuto::getTakeTime).reduce(Integer::sum).orElseThrow(
            () -> new RuntimeException("Вы не выбрали ни одну услугу!!!")
        );
        recording.setEndTime(recording.getStartTime().plusMinutes(takeTime));
        recording.setDateIt(newObject.getDateIt().isBefore(LocalDate.now()) ? LocalDate.now() : newObject.getDateIt());

        if (recordingRepository.findBetweenTime(recording.getDateIt(), recording.getStartTime(),
            recording.getEndTime()) > 0) {
            log.error("RecordingService.createFromDTO: Не возможна запись на данное время");
            throw new RuntimeException("Не возможна запись на данное время");
        }

        recording.setUser(userRepository.findById(newObject.getUser()).orElseThrow(
            () -> new NotFoundException("User with id = " + newObject.getUser() + " not found!")
        ));
        recording.setService(serviceAutos);
        recording.setCreatedWhen(LocalDateTime.now());

        return recordingRepository.save(recording);
    }

    @Override
    public void delete(final Long objectId) {
        Recording recording = getOne(objectId);
        recordingRepository.delete(recording);
        log.info(".delete: удалена запись с id = " + objectId);
    }

    @Override
    public Recording getOne(final Long objectId) {
        Recording recording = recordingRepository.findById(objectId).orElseThrow(
            () -> new NotFoundException("Recording with id = " + objectId + " not found!")
        );
        return recording;
    }

    @Override
    public List<Recording> listAll() {
        return recordingRepository.findAllbyDate(LocalDate.now());
    }

    public RecordingWithNamesAndRemainTimeDTO getOneRecordingWNART(final Long id) {
        Recording recording = getOne(id);
        RecordingWithNamesAndRemainTimeDTO remainTimeDTO = new RecordingWithNamesAndRemainTimeDTO(recording);
        remainTimeDTO.setUser(recording.getUser().getLastName() + " " + recording.getUser().getFirstName());
        Set<String> serviceAutos =
            recording.getService().stream().map(ServiceAuto::getTitle).collect(Collectors.toSet());
        remainTimeDTO.setServices(serviceAutos);

        Integer remainTime = remainTime(recording.getStartTime(), recording.getDateIt());

        if (remainTime < 0) {
            throw new RecordingException("Время записи истекло!");
        }

        remainTimeDTO.setRemainTime(remainTime);
        return remainTimeDTO;
    }

    private Integer remainTime(final LocalTime startTime, final LocalDate date) {
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime timeRecord = LocalDateTime.of(date, startTime);
        long epochRecord = timeRecord.atZone(zoneId).toEpochSecond();
        long epochNow = LocalDateTime.now().atZone(zoneId).toEpochSecond();
        int result = (int) (epochRecord - epochNow) / 60;
        return result;
    }

    private Integer remainTime(final LocalTime startTime, final LocalDate date, final LocalTime endTime) {
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime timeRecord = LocalDateTime.of(date, startTime);
        long epochStart = timeRecord.atZone(zoneId).toEpochSecond();
        long epochEnd = LocalDateTime.of(date, endTime).atZone(zoneId).toEpochSecond();
        int result = (int) (epochEnd - epochStart) / 60;
        return result;
    }

    public List<TimeRecordDTO> listFreeTime(LocalDate date) {
        List<TimeRecordDTO> recordDTOS = listBookTime(date);
        Map<TimeRecordDTO, Integer> resultMap = new LinkedHashMap<>();
        List<TimeRecordDTO> result = new ArrayList<>();
        if (recordDTOS.size() == 0) {
            resultMap.put(TimeRecordDTO.builder()
                .dateIt(date)
                .startTime(LocalTime.of(6, 0))
                .endTime(LocalTime.of(22, 0))
                .build(), ((22 - 6) * 60));
        } else {
            for (int i = 0; i < recordDTOS.size(); i++) {
                if (i == 0) {
                    resultMap.put(TimeRecordDTO.builder()
                        .dateIt(date)
                        .startTime(LocalTime.of(6, 0))
                        .endTime(recordDTOS.get(i).getStartTime())
                        .build(), remainTime(LocalTime.of(6, 0), date, recordDTOS.get(i).getStartTime()));

                    LocalTime lastTime =
                        i == recordDTOS.size() - 1 ? LocalTime.of(22, 0) : recordDTOS.get(i + 1).getStartTime();

                    resultMap.put(TimeRecordDTO.builder()
                            .dateIt(date)
                            .startTime(recordDTOS.get(i).getEndTime())
                            .endTime(lastTime)
                            .build(),
                        remainTime(recordDTOS.get(i).getEndTime(), date, lastTime));
                } else if (i == recordDTOS.size() - 1) {
                    resultMap.put(TimeRecordDTO.builder()
                        .dateIt(date)
                        .startTime(recordDTOS.get(i).getEndTime())
                        .endTime(LocalTime.of(22, 0))
                        .build(), remainTime(recordDTOS.get(i).getEndTime(), date, LocalTime.of(22, 0)));

                } else {
                    resultMap.put(TimeRecordDTO.builder()
                            .dateIt(date)
                            .startTime(recordDTOS.get(i).getEndTime())
                            .endTime(recordDTOS.get(i + 1).getStartTime())
                            .build(),
                        remainTime(recordDTOS.get(i).getEndTime(), date, recordDTOS.get(i + 1).getStartTime()));
                }
            }
        }

        for (Map.Entry<TimeRecordDTO, Integer> entry : resultMap.entrySet()) {
            //TODO: Минимальная услуга по времени 10 минут
            // можно сделать выборку из базы по минимальному времени
            if (entry.getValue() > 9) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    public List<TimeRecordDTO> listBookTime(LocalDate date) {
        List<Recording> records = recordingRepository.findAllbyDate(date);
        List<TimeRecordDTO> recordDTOS = new ArrayList<>();
        for (Recording record : records) {
            recordDTOS.add(new TimeRecordDTO(record));
        }
        return recordDTOS;
    }
}
