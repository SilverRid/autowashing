package com.test.autowashing.service;

import java.util.ArrayList;
import java.util.List;
import com.test.autowashing.model.User;
import com.test.autowashing.model.UserSecurityModel;
import com.test.autowashing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Service
public class UserServiceDetails implements UserDetailsService {
    @Value("${spring.security.user.name}")
    private String adminUserName;

    @Value("${spring.security.user.password}")
    private String adminUserPassword;

    @Value("${spring.security.user.roles}")
    private String adminUserRoles;

    private final UserRepository userRepository;

    public UserServiceDetails(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        if (username.equals(adminUserName)) {
            return org.springframework.security.core.userdetails.User
                .builder()
                .username(username)
                .password(adminUserPassword)
                .roles(adminUserRoles)
                .build();
        } else {
            User user = userRepository.findByLogin(username);
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(user.getRole().getTitle()));
            return new UserSecurityModel(user.getId().intValue(), username, user.getPassword(), authorities);
        }
    }
}
