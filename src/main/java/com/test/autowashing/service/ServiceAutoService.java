package com.test.autowashing.service;


import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.test.autowashing.dto.ServiceAutoDTO;
import com.test.autowashing.exeption.RecordingException;
import com.test.autowashing.exeption.ServiceAutoException;
import com.test.autowashing.model.Recording;
import com.test.autowashing.model.ServiceAuto;
import com.test.autowashing.repository.ServiceRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Log4j2
@Service
public class ServiceAutoService extends GenericService<ServiceAuto, ServiceAutoDTO>{

    private ServiceRepository serviceRepository;

    public ServiceAutoService(final ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @Override
    public ServiceAuto updateFromEntity(final ServiceAuto object) {
        return serviceRepository.save(object);
    }

    @Override
    public ServiceAuto updateFromDTO(final ServiceAutoDTO object, final Long objectId) {
        ServiceAuto serviceAuto = getOne(objectId);
        serviceAuto.setTitle(object.getTitle());
        serviceAuto.setDescription(object.getDescription());
        serviceAuto.setPrice(object.getPrice());
        serviceAuto.setTakeTime(object.getTakeTime());
        return serviceRepository.save(serviceAuto);
    }

    @Override
    public ServiceAuto createFromEntity(final ServiceAuto newObject) {
        return serviceRepository.save(newObject);
    }

    @Override
    public ServiceAuto createFromDTO(final ServiceAutoDTO newObject) {
        ServiceAuto serviceAuto = new ServiceAuto();
        serviceAuto.setCreatedWhen(LocalDateTime.now());
        serviceAuto.setTitle(newObject.getTitle());
        serviceAuto.setDescription(newObject.getDescription());
        serviceAuto.setPrice(newObject.getPrice());
        serviceAuto.setTakeTime(newObject.getTakeTime());
        return serviceRepository.save(serviceAuto);
    }

    @Override
    public void delete(final Long objectId) {
        ServiceAuto serviceAuto = serviceRepository.findById(objectId).orElseThrow(
            () -> new NotFoundException("Service such id = " + objectId + " not Found!")
        );
        log.info(serviceAuto.getRecordings());
        if (serviceAuto.getRecordings().size() > 0) {
            throw new ServiceAutoException("{\"info\":\"Для удаления сервиса удалите все связанные записи с ним!\"}");
        }
        serviceRepository.delete(serviceAuto);
        log.info("ServiceAutoService.delete удален сервис с id = " + objectId);
    }

    @Override
    public ServiceAuto getOne(final Long objectId) {
        ServiceAuto serviceAuto = serviceRepository.findById(objectId).orElseThrow(
            () -> new NotFoundException("Service such id = " + objectId + " not Found!")
        );
        return serviceAuto;
    }

    @Override
    public List<ServiceAuto> listAll() {
        return serviceRepository.findAll();
    }

    public Set<ServiceAuto> setFromListId(final Set<Long> service) {
        Set<ServiceAuto> serviceAutos = new HashSet<>();
        for (Long id: service) {
            serviceAutos.add(getOne(id));
        }
        return serviceAutos;
    }
}
