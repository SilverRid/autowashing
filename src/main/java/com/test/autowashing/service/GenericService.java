package com.test.autowashing.service;

import java.util.List;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
public abstract class GenericService<T, N> {

    public abstract T updateFromEntity(T object);

    public abstract T updateFromDTO(N object, Long objectId);

    public abstract T createFromEntity(T newObject);

    public abstract T createFromDTO(N newObject);

    public abstract void delete(final Long objectId);

    public abstract T getOne(final Long objectId);

    public abstract List<T> listAll();

}
