package com.test.autowashing.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Entity
@Table(name = "recording")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "recording_id_seq", allocationSize = 1, initialValue = 10)
public class Recording extends GenericModel {
    @Column(name = "date_it")
    private LocalDate dateIt;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "end_time")
    private LocalTime endTime;

    @Column(name = "box_number")
    private String boxNumber;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {
        CascadeType.DETACH,
        CascadeType.MERGE,
        CascadeType.REFRESH,
        CascadeType.PERSIST
    })
    @JoinTable(name = "service_recording",
        joinColumns = @JoinColumn(name = "service_id"),
        foreignKey = @ForeignKey(name = "service_recording_service_id_fkey"),
        inverseJoinColumns = @JoinColumn(name = "recording_id"),
        inverseForeignKey = @ForeignKey(name = "service_recording_recording_id_fkey"))
    private Set<ServiceAuto> service;

}
