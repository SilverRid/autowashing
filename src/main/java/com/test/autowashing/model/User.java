package com.test.autowashing.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "users_id_seq", allocationSize = 1, initialValue = 10)
public class User extends GenericModel {

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "date_birth")
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateBirth;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    @JsonIgnore
    private String password;

    @Column(name = "address")
    private String address;

    @Column(name = "backup_email", unique = true)
    private String backupEmail;

    @Column(name = "phone", unique = true)
    private String phone;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id",
    foreignKey = @ForeignKey(name = "users_role_id_fkey"))
    @JsonIgnore
    private Role role;

}
