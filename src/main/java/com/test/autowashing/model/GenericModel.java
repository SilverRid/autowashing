package com.test.autowashing.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class GenericModel
    implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen")
    private Long id;

    @Column(name = "created_when")
    @Schema(example = "2022-01-23 15:00:00", defaultValue = "2022-01-23 15:00:00")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdWhen;

    @Column(name = "created_by")
    private String createdBy;
}