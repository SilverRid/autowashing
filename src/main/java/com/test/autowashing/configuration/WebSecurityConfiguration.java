package com.test.autowashing.configuration;


import com.test.autowashing.service.UserServiceDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration {

    private UserServiceDetails userServiceDetails;

    @Autowired
    public void setUserService(final UserServiceDetails userServiceDetails) {
        this.userServiceDetails = userServiceDetails;
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public HttpFirewall allowPercent() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedPercent(true);
        firewall.setAllowUrlEncodedSlash(true);
        return firewall;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf()
            .disable()
            .authorizeRequests()
            .antMatchers("/",
                "/login",
                "/error/**",
                "/api/users/auth/**")
            .permitAll()
            .and()
            .authorizeRequests()
            .antMatchers("/resourses/**",
                "/static/**",
                "/assets/**",
                "/css/**",
                "/js/**",
                "/image/**",
                "/webjars/**"
            )
            .permitAll()
            .and().authorizeRequests().antMatchers("/recording").hasAnyRole("USER", "ADMIN")
            .and().authorizeRequests().antMatchers("/service/list").hasAnyRole("USER", "ADMIN")
            .and().authorizeRequests().antMatchers("/service/**").hasAnyRole("ADMIN")
            .anyRequest().authenticated()
            .and()
            .formLogin().failureForwardUrl("/login")
            .defaultSuccessUrl("/").permitAll()
            .and()
            .logout().permitAll()
            .logoutSuccessUrl("/login");
        return http.build();

    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder managerBuilder) throws Exception {
        managerBuilder.userDetailsService(userServiceDetails).passwordEncoder(bCryptPasswordEncoder());

    }
}