package com.test.autowashing.controllers;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;
import com.test.autowashing.dto.RecordingDTO;
import com.test.autowashing.dto.RecordingWithNamesAndRemainTimeDTO;
import com.test.autowashing.model.Recording;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RecordingTest extends GenericControllerTest<Recording, RecordingDTO>{

    public static RecordingDTO recordingDTO = new RecordingDTO();

    static {
        recordingDTO.setDateIt(LocalDate.now().plusDays(10));
        recordingDTO.setStartTime(LocalTime.of(7,0));
        recordingDTO.setServices(Set.of(1L,2L));// Требутся создать отдельно услуги для теста
        recordingDTO.setUser(1L);  // Требутся создать отдельно пользователя для теста
        recordingDTO.setBoxNumber("1");

    }
    @Order(1)
    @Test
    @WithMockUser(roles="ADMIN")
    @Override
    public void listTest() throws Exception {
        listTestReal("/recording/list");
    }

    @Test
    @Order(2)
    @WithMockUser(roles="ADMIN")
    @Override
    public void createTest() throws Exception {
        String response = createTestReal("/recording/add", recordingDTO);
        Recording recording = mapperData.readValue(response, Recording.class);
        System.out.println(recording.getId());
        recordingDTO.setId(recording.getId());
    }

    @Test
    @Order(3)
    @WithMockUser(roles="ADMIN")
    @Override
    public void getOneTest() throws Exception {
        String response = getOneTestReal("/recording/get", recordingDTO.getId());

        System.out.println(response);
        RecordingWithNamesAndRemainTimeDTO  remainTimeDTO = mapperData.readValue(response, RecordingWithNamesAndRemainTimeDTO.class);
        System.out.println(remainTimeDTO);
    }
    @Order(4)
    @Override
    public void updateTest() throws Exception {

    }
    @Test
    @WithMockUser(roles="ADMIN")
    @Order(5)
    @Override
    public void deleteTest() throws Exception {
        String response = deleteTestReal("/recording/delete", recordingDTO.getId());
        System.out.println(response);
    }


}
