package com.test.autowashing.controllers;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.test.autowashing.dto.RecordingWithNamesAndRemainTimeDTO;
import com.test.autowashing.dto.ServiceAutoDTO;
import com.test.autowashing.model.Recording;
import com.test.autowashing.model.ServiceAuto;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    @SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
    @AutoConfigureMockMvc
    public class ServiceTest extends GenericControllerTest<ServiceAuto, ServiceAutoDTO>{

    @Value("${spring.security.user.name}")
    private String adminUserName;

    @Value("${spring.security.user.password}")
    private String adminUserPassword;

    @Value("${spring.security.user.roles}")
    private String adminUserRoles;

    private static ServiceAutoDTO serviceAutoDTO = new ServiceAutoDTO(null, "New Service", "Test Service", 12.,12);


    public static RequestPostProcessor userHttpBasic() {
        return user("admin").roles("ADMIN");
    }

    @Test
    @Order(1)
        public void loginTest() throws Exception {
        String response = mockMvc.perform(
                post("/login")
                    .headers(getHttpHeaders())
                    .param("username", "admin")
                    .param("password", "admin")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andReturn()
            .getResponse()
            .getContentAsString();
        System.out.println(response);
    }

    @Test
    @WithMockUser(roles="ADMIN")
    @Order(2)
    @Override
    public void listTest() throws Exception {
        listTestReal("/service/list");
    }
    @Test
    @WithMockUser(roles="ADMIN")
    @Order(3)
    @Override
    public void createTest() throws Exception {
        String response = createTestReal("/service/add", serviceAutoDTO);
        ServiceAuto serviceAuto = mapperData.readValue(response, ServiceAuto.class);
        System.out.println(serviceAuto.getId());
        serviceAutoDTO.setId(serviceAuto.getId());
    }
    @Test
    @WithMockUser(roles="ADMIN")
    @Order(4)
    @Override
    public void getOneTest() throws Exception {
        String response = getOneTestReal("/service/get", serviceAutoDTO.getId());

        System.out.println(response);
        ServiceAuto
            serviceAuto = mapperData.readValue(response, ServiceAuto.class);
        System.out.println(serviceAuto);
    }
    @Order(5)
    @Override
    public void updateTest() throws Exception {

    }
    @Test
    @WithMockUser(roles="ADMIN")
    @Order(6)
    @Override
    public void deleteTest() throws Exception {
        String response = deleteTestReal("/service/delete", serviceAutoDTO.getId());
        System.out.println(response);
    }
}
