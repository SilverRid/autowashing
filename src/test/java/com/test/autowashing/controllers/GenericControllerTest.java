package com.test.autowashing.controllers;

import java.util.List;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */
public abstract class GenericControllerTest<T, D> {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper mapperData;

    protected static final String ROLE_ADMIN_USERNAME = "gvalov";

    protected String token;

    protected HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        return httpHeaders;
    }


    public String asJsonString(Object obj) {
        try {
            return mapperData.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public abstract void listTest() throws Exception;

    public abstract void createTest() throws Exception;

    public abstract void getOneTest() throws Exception;

    public abstract void updateTest() throws Exception;

    public abstract void deleteTest() throws Exception;

    protected void listTestReal(String path) throws Exception {
        String result = mockMvc.perform(
                get(path)
                    .headers(getHttpHeaders())
                    .with(httpBasic("admin","admin"))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)

            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        //  Добавление json маппера по дата паттерну
//        ObjectMapper mapper = new ObjectMapper();
//        JavaTimeModule javaTimeModule = new JavaTimeModule();
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dateTimeFormatter));
//        mapper.registerModule(javaTimeModule);


        List<T> list = mapperData.readValue(result, new TypeReference<List<T>>() {
        });
        System.out.println(list.size());
        System.out.println(result);
    }

    public String createTestReal(String path, D obj) throws Exception {
        String response = mockMvc.perform(
                post(path)
                    .headers(getHttpHeaders())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(obj))
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn()
            .getResponse()
            .getContentAsString();

        System.out.println(response);

        return response;
    }

    public String getOneTestReal(String path, Long id) throws Exception {
        String response = mockMvc.perform(
                get(path)
                    .param("id", id.toString())
                    .headers(getHttpHeaders())
                    .contentType(MediaType.APPLICATION_JSON)
//                    .content(asJsonString(obj))
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn()
            .getResponse()
            .getContentAsString();

        return response;
    }

    public String updateTestReal(String path, D obj, Long id) throws Exception {
        String response = mockMvc.perform(
                put(path)
                    .param("id", id.toString())
                    .headers(getHttpHeaders())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(obj))
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn()
            .getResponse()
            .getContentAsString();
        return response;
    }

    public String deleteTestReal(String path, Long id) throws Exception {
        String response = mockMvc.perform(
                delete(path)
                    .param("id", id.toString())
                    .headers(getHttpHeaders())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().is2xxSuccessful())
            .andReturn()
            .getResponse()
            .getContentAsString();

        return response;
    }
}
